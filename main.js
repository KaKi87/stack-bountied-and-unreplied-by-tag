import { parse as parseArgs } from 'https://deno.land/std@0.145.0/flags/mod.ts';
import dayjs from 'https://dev.jspm.io/npm:dayjs@1.11.3';
import LocalizedFormat from 'https://dev.jspm.io/npm:dayjs@1.11.3/plugin/localizedFormat';
import AsciiTable from 'https://dev.jspm.io/npm:ascii-table@0.0.9';

dayjs.extend(LocalizedFormat);

const
    {
        site = 'stackoverflow',
        page = '1',
        'tags': stringTags
    } = parseArgs(Deno.args),
    baseUrl = 'https://api.stackexchange.com',
    questionsUrl = new URL(
        '/2.3/questions/featured',
        baseUrl
    ),
    table = new AsciiTable().setHeading(
        'Match',
        'Bounty',
        'Bounty exp.',
        'Tags',
        'Link'
    ),
    tags = stringTags ? stringTags.split(',') : [];

Object.entries({
    site,
    page,
    'pagesize': '100',
    'order': 'desc',
    'sort': 'creation'
}).forEach(([key, value]) => questionsUrl.searchParams.set(key, value));

for(
    const questionItem
    of
    (await (await fetch(questionsUrl)).json())['items']
        .filter(item => item['answer_count'] === 0)
        .map(item => ({
            id: item['question_id'],
            tagsMatch: item['tags'].filter(tag => tags.includes(tag)).length / item['tags'].length,
            bountyAmount: item['bounty_amount'],
            bountyExpirationTimestamp: item['bounty_closes_date'] * 1000,
            tags: item['tags'],
            link: item['link']
        }))
        .filter(item => item.tagsMatch > 0 || !tags.length)
        .sort((a, b) =>
            b.tagsMatch - a.tagsMatch
            ||
            b.bountyAmount - a.bountyAmount
            ||
            a.bountyExpirationTimestamp - b.bountyExpirationTimestamp
        )
){
    const commentsUrl = new URL(
        `/2.3/questions/${questionItem.id}/comments`,
        baseUrl
    );
    commentsUrl.searchParams.set('site', site);
    const commentCount = (await (await fetch(commentsUrl)).json())['items'].length;
    if(commentCount === 0)
        table.addRow(
            `${Math.round(questionItem.tagsMatch * 100)}%`,
            questionItem.bountyAmount,
            dayjs(questionItem.bountyExpirationTimestamp).format('L'),
            questionItem.tags.join(' '),
            questionItem.link
        );
}

console.log(table.toString());